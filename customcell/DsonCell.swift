//
//  DsonCell.swift
//  customcell
//
//  Created by kawase yu on 2015/07/16.
//  Copyright (c) 2015年 kawase. All rights reserved.
//

import UIKit

class DsonCell: UITableViewCell {

    private let iconView = UIImageView(frame: CGRectMake(10, 10, 80, 80))
    private let nameLabel = UILabel(frame: CGRectMake(100, 10, 210, 20))
    private let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
    
    func setup(){
        indicator.frame = iconView.frame
        
        iconView.contentMode = UIViewContentMode.ScaleAspectFill
        iconView.backgroundColor = UIColorFromHex(0x555555)
        iconView.clipsToBounds = true
        
        nameLabel.font = UIFont.boldSystemFontOfSize(20)
        
        let v = self.contentView
        v.addSubview(iconView)
        v.addSubview(indicator)
        v.addSubview(nameLabel)
    }
    
    func reload(data:NSDictionary){
        nameLabel.text = data["name"] as? String
        
        
        // 画像を読みに行く
        let imageUrl:String! = data["imageUrl"] as! String
        iconView.image = nil // 一回クリア
        indicator.startAnimating()
        AppUtil.loadImage(imageUrl, callback: { (image, key, useCache) -> () in
            if key != imageUrl{ // 読みに行った画像と、現在の画像が違う時はスルー
                return
            }
            
            self.iconView.image = image
            self.indicator.stopAnimating()
        })
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
