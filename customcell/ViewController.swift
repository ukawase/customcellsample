//
//  ViewController.swift
//  customcell
//
//  Created by kawase yu on 2015/07/16.
//  Copyright (c) 2015年 kawase. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var dataList:[NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataList = [
            [
                "name":"dson"
                , "imageUrl": "http://docs.daz3d.com/lib/exe/fetch.php/public/software/dson_importer/poser/dson-importer_poser.png"
                , "age": 21
            ],
            [
                "name": "umekikkusu"
                , "imageUrl": "https://s3-ap-northeast-1.amazonaws.com/i.schoo/images/teacher/152.jpg"
                , "age": 21
            ]
        ]
        
        let tableView = UITableView(frame: CGRectMake(0, 0, 320, 568))
        tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0)
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let CELL_IDENTIFIER = "DSON_CELL"
        
        var cell:DsonCell? = tableView.dequeueReusableCellWithIdentifier(CELL_IDENTIFIER) as! DsonCell?
        if cell == nil {
            cell = DsonCell(style:UITableViewCellStyle.Default, reuseIdentifier:CELL_IDENTIFIER)
            cell!.setup()
        }
        
        let data = dataList[indexPath.row]
        cell!.reload(data)
       
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }


}

